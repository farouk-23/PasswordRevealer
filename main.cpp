/*
 * PasswordRevealer:
 *    Changes password text box to normal edit text box.
 * 
 * windres -i resource.rc -o resource.o
 * gcc -D UNICODE -D _UNICODE -c main.cpp -o pass.o
 * gcc -o PasswordRevealer.exe pass.o resource.o -s -lcomctl32 -Wl,--subsystem,windows
 *
 */

#include <windows.h>
#include <CommCtrl.h>
#include "resource.h"

//#pragma comment(lib, "ComCtl32.lib")

/*  Declare Windows procedure  */
LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);
BOOL WINAPI EnumWindowsProc(HWND hWnd, LPARAM lParam);
BOOL WINAPI EnumWindowsProc2(HWND hWnd, LPARAM lParam);

/*  Make the class name into a global variable  */
char szClassName[] = "PasswordRevealer";

int WINAPI WinMain (HINSTANCE hInstance,
                    HINSTANCE hPrevInstance,
                    LPSTR lpszArgument,
                    int nShowCmd)
{
    HWND hWnd;               /* This is the handle for our window */
    MSG messages;            /* Here messages to the application are saved */
    WNDCLASSEX wincl;        /* Data structure for the windowclass */

    InitCommonControls();
    /* The Window structure */
    wincl.hInstance = hInstance;
    wincl.lpszClassName = LPCWSTR(szClassName);
    wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
    wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
    wincl.cbSize = sizeof (WNDCLASSEX);

    /* Use default icon and mouse-pointer */
    wincl.hIcon = LoadIcon (hInstance, MAKEINTRESOURCE(IDI_APP_ICON));
    wincl.hIconSm = LoadIcon (hInstance, MAKEINTRESOURCE(IDI_APP_ICON));
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL;                 /* No menu */
    wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
    wincl.cbWndExtra = 0;                      /* structure or the window instance */
    /* Use Windows's default color as the background of the window */
    wincl.hbrBackground = (HBRUSH) COLOR_BTNSHADOW;

    /* Register the window class, and if it fails quit the program */
    if (!RegisterClassEx (&wincl))
        return 0;
    /* The class is registered, let's create the program*/
    hWnd = CreateWindowEx (
        0,                   /* Extended possibilites for variation */
        LPCWSTR(szClassName),/* Classname */
        LPCWSTR(" - Password Revealer - "),       /* Title Text */
        WS_POPUP | WS_CAPTION | WS_SYSMENU | SS_CENTER , /* Window style */
        CW_USEDEFAULT,       /* Windows decides the position */
        CW_USEDEFAULT,       /* where the window ends up on the screen */
        250,                 /* The programs width */
        150,                 /* and height in pixels */
        HWND_DESKTOP,        /* The window is a child-window to desktop */
        NULL,                /* No menu */
        hInstance,           /* Program Instance handler */
        NULL                 /* No Window Creation data */
        );

    /* Set window position at screen center */
    RECT rc;
    GetWindowRect ( hWnd, &rc ) ;
    int xPos = (GetSystemMetrics(SM_CXSCREEN) - rc.right)/2;
    int yPos = (GetSystemMetrics(SM_CYSCREEN) - rc.bottom)/2;
    SetWindowPos( hWnd, 0, xPos, yPos, 0, 0, SWP_NOZORDER | SWP_NOSIZE );

    CreateWindowExA( 0,
        "BUTTON",
        "Reveal Passwords",
        WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
        40, 70, 170, 26,
        hWnd, 0, hInstance, 0);
        
    CreateWindowExA( 0,
        "STATIC",
        "Click below to reveal any hidden on-screen passwords",
        WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
        25, 10, 200, 60,
        hWnd, 0, hInstance, 0);

    /* Make the window visible on the screen */
    ShowWindow (hWnd, nShowCmd);

    /* Run the message loop. It will run until GetMessage() returns 0 */
    while (GetMessage (&messages, NULL, 0, 0))
    {
        /* Translate virtual-key messages into character messages */
        TranslateMessage(&messages);
        /* Send message to WindowProcedure */
        DispatchMessage(&messages);
    }
    /* The program return-value is 0 - The value that PostQuitMessage() gave */
    return messages.wParam;
}


/*  This function is called by the Windows function DispatchMessage()  */
LRESULT CALLBACK WindowProcedure (HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
  if ( message == WM_DESTROY ) /* send a WM_QUIT to the message queue */ 
  {
    PostQuitMessage(0);
    return 0;
  }
  if ( message == WM_COMMAND ) /* Button clicked */
  {
    if ( !HIWORD(wParam) )
      EnumWindows( EnumWindowsProc, 0);
    return 0;
  }
  
  /* for messages that we don't deal with */
  return DefWindowProcA(hWnd, message, wParam, lParam);
}

BOOL WINAPI EnumWindowsProc(HWND hWnd, LPARAM lParam)
{
  EnumChildWindows(hWnd, EnumWindowsProc2, lParam);
  return 1;
}

BOOL WINAPI EnumWindowsProc2(HWND hWnd, LPARAM lParam)
{
  SendMessageA( hWnd, EM_SETPASSWORDCHAR, 0, 0);
  InvalidateRect( hWnd, 0, 0);
  return 1;
}
